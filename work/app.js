angular.module('app', [])
    .controller('AppController', ['$scope', function($scope) {

        $scope.persons = [
            {
                id: 1,
                name: "John",
                surname: "Doe",
                age: 27,
                deleted: false
            }
        ];

        $scope.cloningPersons = [
            {
                id: 1,
                name: "Ben",
                surname: "Fredericks",
                age: 34,
                deleted: false
            },
            {
                id: 2,
                name: "Dan",
                surname: "Brown",
                age: 28,
                deleted: false
            },
            {
                id: null,
                name: "",
                surname: "",
                age: null,
                deleted: false
            },
            {
                id: null,
                name: "",
                surname: "",
                age: null,
                deleted: false
            },
            {
                id: null,
                name: "",
                surname: "",
                age: null,
                deleted: false
            }
        ];

        $scope.removeRow = function( person ) {
            var index = $scope.persons.indexOf( person );
            if ( $scope.persons[ index ].id ){
                $scope.persons[ index ].deleted = true;
            } else {
                $scope.persons.splice(index, 1);
            }
        };

        $scope.restoreRow = function( person ) {
            var index = $scope.persons.indexOf( person );
            $scope.persons[ index ].deleted = false;
        };

    }])
    .directive('jpAddARow', function() {

        var template = '<button class="btn btn-primary btn-sm" id="btnAddARow" title="Add a row"><span class="glyphicon glyphicon-plus"></span></button>';

        var link = function ($scope, $element) {

            $element.on('click', function() {

                var emptyPersonObject = {
                    id: null,
                    name: "",
                    surname: "",
                    age: null,
                    deleted: false
                };

                $scope.persons.push(emptyPersonObject);
                $scope.$apply();

            });


        };

        return {
            scope: true,
            template: template,
            link: link,
            replace: true
        };
    })
    .directive('jpTwoDigitNumber', function() {

        var link = function( $scope, $element, $attrs, ngModel ) {

            ngModel.$parsers.unshift( function( value ) {

                if ( value !== '' && value !== null ) {
                    value = ( /^[+]?\d+(\.\d{1,2})?$/.test( value ) ) ? value : null
                    ngModel.$setValidity( 'jp-two-digit-number', !!value );
                } else {
                    value = null;
                    ngModel.$setValidity( 'jp-two-digit-number', true );
                }

                return value;
            });

        };

        return {
            require: 'ngModel',
            link: link
        };

    })
    .directive('jpEnterAsTabDown', function () {

        var link = function ($scope, $element) {

            $element.on("keydown keypress", function (event) {
                if( event.which === 13 ) {
                    event.preventDefault();

                    var columnIndex = 0;
                    var elementToFocus = $element.parent().parent().next().find('input')[columnIndex];

                    if(angular.isDefined(elementToFocus)) {
                        elementToFocus.focus();
                    }
                }
            });
        };

        return {
            link: link
        };

    })
    // CLONE DOWN BUTTON
    .directive('jpCloneDown', function () {

        var link = function ($scope, $element, $attrs) {

            $element.on('click', function() {
                var clone_object = $attrs.cloneObject; // eg: person
                var clone_column = $attrs.cloneColumn; // eg: name
                var clone_safe = $attrs.cloneSafe;
                var scope_reference = $scope[ clone_object ];

                // Note: This is not used anymore because hole numbers are not ordered on the page anymore
                // we now clone down as is!
                //scope_reference = _.sortBy( scope_reference, 'holeNumber' );

                var amount_of_rows = scope_reference.length;
                var row_index = parseInt($attrs.rowIndex);
                var start_index = row_index + 1;

                for (var i = start_index; i < amount_of_rows; i++) {
                    if( clone_safe ) {
                        if (!(scope_reference[i][clone_column])) {
                            clone_the_data(scope_reference, clone_column, i, row_index);
                            console.log("JpCloneDown: Cloning '", scope_reference[row_index][clone_column], "' INTO '", clone_column, "' @ row ", i + 1);
                        }
                        else {
                            break;
                        }
                    } else {
                        clone_the_data(scope_reference, clone_column, i, row_index);
                        console.log("JpCloneDown: Cloning '", scope_reference[row_index][clone_column], "' INTO '", clone_column, "' @ row ", i + 1);
                    }
                }
                $scope.$apply();

            });

            function clone_the_data( scope_reference, clone_column, i, row_index ) {
                scope_reference[ i ][ clone_column ] = scope_reference[ row_index ][ clone_column ];
            }
        };

        var template = '<button class="btn btn-sm btn-warning" title="Clone down"><span class="glyphicon glyphicon-arrow-down"></span></button>';

        return {
            link: link,
            template: template,
            replace: true,
            scope: true
        };

    })
