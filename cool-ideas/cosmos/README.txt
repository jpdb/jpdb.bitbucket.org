A Pen created at CodePen.io. You can find this one at http://codepen.io/captainbrosset/pen/JoZabN.

 CSS-only.
Inspired by https://dribbble.com/shots/1546247-COSMOS-Identity-GIF

- vw, vh, vmin units for responsiveness
- flexbox to center everything
- multiple box-shadows for the stars
- keyframes animation for the planets
- transform to rotate the planets